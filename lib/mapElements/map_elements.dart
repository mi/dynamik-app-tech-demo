import 'package:mapsforge_flutter/core.dart';
import 'package:mapsforge_flutter/src/datastore/datastorereadresult.dart';
import 'package:mapsforge_flutter/src/model/tag.dart';

//saves all PointsOfInterest (Poi) and Ways of the json response file
class MapElements {
  List<Poi> pois = List.empty(growable: true);
  List<Way> ways = List.empty(growable: true);

  MapElements({
    required this.pois,
    required this.ways,
  });

  @override
  String toString() {
    return 'Elements: ${pois.toString()}, ${ways.toString()}';
  }

  MapElements.fromDatastoreReadResult(
      DatastoreReadResult datastoreReadResult, Map<String, dynamic>? filter) {
    String poiListText = ('Number of PointsOfInterest:  '
        '${datastoreReadResult.pointOfInterests.length}\n\n');
    int i = 1;
    for (final poi in datastoreReadResult.pointOfInterests) {
      /*if (i > 90) {
        poiListText = ('$poiListText' + '. . . (there are more than 90 poi)\n\n');
        break;
      }*/
      bool containsAllTags = true;
      filter?.forEach((key, value) {
        if (!(poi.tags.contains(Tag(key, value)))) {
          containsAllTags = false;
        }
      });
      //filter Tags                                                                            11111111111111111111111111
      // poiListText = ('$poiListText' + 'Poi $i: ${poi.toString()}\n\n');

      if (containsAllTags)
        pois.add(Poi(
            latitude: poi.position.latitude,
            longitude: poi.position.longitude,
            tags: tagsToMap(poi.tags)));
      //i++;
    }
    for (final way in datastoreReadResult.ways) {
      /*if (i > 90) {
        poiListText = ('$poiListText' + '. . . (there are more than 90 poi)\n\n');
        break;
      }*/
      bool containsAllTags = true;
      filter?.forEach((key, value) {
        if (!(way.tags.contains(Tag(key, value)))) {
          containsAllTags = false;
        }
      });
      // poiListText = ('$poiListText' + 'Poi $i: ${poi.toString()}\n\n');

      if (containsAllTags) {
        ways.add(Way(
            path: way.latLongs.first,
            polygon: searchPolygonFromList(way.latLongs.first),
            tags: tagsToMap(way.tags)));
      }
      //i++;
    }
  }

  bool searchPolygonFromList(List<ILatLong> LatLongList) {
    if (LatLongList.first == LatLongList.last)
      return true;
    else
      return false;
  }

  Map<String, dynamic>? tagsToMap(List<Tag> tagList) {
    Map<String, dynamic>? tagMap = {};

    tagList.forEach((element) {
      tagMap.addAll({element.key!: element.value});
    });
    return tagMap;
  }

  MapElements.fromJson(List<dynamic> json, Map<String, dynamic>? filter) {
    for (var element in json) {
      if (element['tags'] != null) {
        //skips all nodes without a Tag

        if (element['nodes'] == null) {
          bool containsAllTags = false;

          Map<String, dynamic>? tags = element['tags'];

          filter?.forEach((key1, value1) {
            tags?.forEach((key2, value2) {
              if (key1 == key2 && value1 == value2) {
                containsAllTags = true;
              }
            });
          });
          if (containsAllTags)
            pois.add(Poi(
                latitude: element['lat'],
                longitude: element['lon'],
                tags: element['tags']));
        } else {
          ways.add(Way(
              tags: element['tags'],
              path: searchPath(json, element),
              polygon: searchPolygon(element['nodes'])));
        }
      }
    }

    //it is possible to search further tags here
    //json['tags']['natural'] != null
    //json['tags']['addr:street'];
  }

  List<ILatLong> searchPath(List<dynamic> json, var element) {
    List<ILatLong> path = List.empty(growable: true);
    for (var nodeID in element['nodes']) {
      for (var element in json) {
        if (element['tags'] == null) {
          if (element['nodes'] == null) {
            if (element['id'] == nodeID) {
              path.add(LatLong(element['lat'], element['lon']));
            }
          }
        }
      }
    }
    return path;
  }

  bool searchPolygon(var element) {
    var firstNode = null, lastNode = null;
    for (var nodeID in element) {
      firstNode ??= nodeID;
      lastNode = nodeID;
    }
    return (firstNode == lastNode);
  }
}

List<String> MapElementsToStringList(MapElements? mapElements) {
  List<String> elementList = List.empty(growable: true);
  int index = 0; //for numeration of the elements

  if (mapElements != null) {
    mapElements.pois.forEach((element) {
      index++;
      elementList.add("${index}: ${element}");
    });
    mapElements.ways.forEach((element) {
      index++;
      elementList.add('${index}: ${element}');
    });
  } else
    elementList.add("no data found");

  return elementList;
}

class Poi {
  final double latitude;
  final double longitude;
  final Map<String, dynamic>? tags;

  Poi({
    required this.latitude,
    required this.longitude,
    required this.tags,
  });

  @override
  String toString() {
    return 'Point: ${tags}'; //Latitude: ${latitude}, Longitude: ${longitude}, Tags:
  }
}

class Way {
  List<ILatLong> path;
  final Map<String, dynamic>? tags;
  final bool polygon; //true - closed, room //false - open, way

  Way({required this.path, required this.tags, required this.polygon});

  @override
  String toString() {
    //the LatLon's of the path are omitted here, because they can be really long and affect readability
    return 'Way: ${tags}'; //Room: ${polygon}, Tags:
  }
}
