import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:logging/logging.dart';
import 'package:mapsforge_example/widgets/pre-start.dart';

import 'map-file-data.dart';

/// The global variable that holds a list of map files.
///
/// Data can be files with distinct places as [MapFileData]
/// or parts of a huge and extensible area as [MapFileData.online].
// ignore: non_constant_identifier_names
final List<MapFileData> MAP_FILE_DATA_LIST = [
  const MapFileData(
    url: "http://andx.eu/share/indoorUB-ext2.map",
    fileName: "indoorUB-ext2.map",
    displayedName: "Chemnitz - Library (Indoor)",
    initialPositionLat: 50.84160,
    initialPositionLong: 12.92700,
    initialZoomLevel: 18,
    indoorZoomOverlay: true,
    indoorLevels: {4: 'OG4', 3: 'OG3', 2: 'OG2', 1: 'OG1', 0: 'EG', -1: 'UG1'},
  )
];

void main() async {
  await Hive.initFlutter();
  await Hive.openBox("settings");
  runApp(MyApp());
}

/// This is the entry point, the main application widget.
class MyApp extends StatelessWidget {
  MyApp() {
    _initLogging();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mapsforge Example App',
      //home: MapList(MAP_FILE_DATA_LIST),
      home: preStart(MAP_FILE_DATA_LIST),
    );
  }

  /// Sets a [Logger] to log debug messages.
  void _initLogging() {
    // Print output to console.
    Logger.root.onRecord.listen((LogRecord r) {
      print('${r.time}\t${r.loggerName}\t[${r.level.name}]:\t${r.message}');
    });

    // Root logger level.
    Logger.root.level = Level.FINEST;
  }
}
