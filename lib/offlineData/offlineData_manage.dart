import 'dart:async';

import 'package:mapsforge_flutter/core.dart';
import 'package:mapsforge_flutter/maps.dart';

import '../map-file-data.dart';

import 'package:mapsforge_example/filemgr.dart';
import 'package:mapsforge_example/pathhandler.dart';
import 'package:mapsforge_flutter/src/utils/layerutil.dart';
import 'package:mapsforge_flutter/src/model/tile.dart';
import 'package:mapsforge_flutter/src/datastore/datastorereadresult.dart';
import '../mapElements/map_elements.dart';

class OfflineData {
  final MapFileData mapFileData;
  final ViewModel viewModel;

  List<Tile>? tileList;
  MapViewPosition? mapViewPosition;
  BoundingBox? boundingBoxView;

  OfflineData(this.mapFileData, this.viewModel,
      {this.mapViewPosition, this.tileList, this.boundingBoxView}) {
    mapViewPosition = viewModel.mapViewPosition;
    tileList = LayerUtil.getTiles(
        viewModel, mapViewPosition!, DateTime.now().millisecondsSinceEpoch);
    boundingBoxView =
        mapViewPosition!.calculateBoundingBox(viewModel.viewDimension!);
  }

  Future<MapElements?> getElementsInBoundingBox(
      Map<String, dynamic>? filter) async {
    filter ??= {};

    return MapElements.fromDatastoreReadResult(
        await _getDatastoreReadResult(await _loadMapFile()), filter);
  }

  /*
  mapViewPosition!.zoomLevel
  mapViewPosition!.indoorLevel
  boundingBoxView!.minLatitude
  boundingBoxView!.maxLatitude
  boundingBoxView!.minLongitude
  boundingBoxView!.maxLongitude
  */

  Future<MapFile> _loadMapFile() async {
    PathHandler pathHandler = await FileMgr().getLocalPathHandler("");
    MapFile mapFile = await MapFile.from(
        pathHandler.getPath(mapFileData.fileName), null, null);
    return mapFile;
  }

  Future<DatastoreReadResult> _getDatastoreReadResult(MapFile mapFile) async {
    //TODO: search if there is a better way to get upper-left and lower-right tile
    Tile upperLeft = tileList![0], lowerRight = tileList![0];

    tileList!.forEach((Tile tile) {
      if (upperLeft.tileX >= tile.tileX && upperLeft.tileY >= tile.tileY)
        upperLeft = tile;
      if (lowerRight.tileX <= tile.tileX && lowerRight.tileY <= tile.tileY)
        lowerRight = tile;
    });
    return await mapFile.readLabels(upperLeft, lowerRight);
  }
}
