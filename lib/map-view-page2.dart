//----------------------------------------------------------------------------->
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mapsforge_example/mapfileanalyze/viewpage.dart';
import 'package:mapsforge_example/marker/marker_datastore.dart';
import 'package:mapsforge_example/marker/marker_datastore_routing_path.dart';
import 'package:mapsforge_example/markerdemo-contextmenubuilder.dart';
import 'package:mapsforge_example/markerdemo-datastore.dart';
import 'package:mapsforge_example/widgets/ble.dart';
import 'package:mapsforge_example/widgets/overlay.dart';
import 'package:mapsforge_example/widgets/postionButton.dart';
import 'package:mapsforge_example/widgets/searchWidget.dart';
import 'package:mapsforge_flutter/core.dart';
import 'package:mapsforge_flutter/datastore.dart';
import 'package:mapsforge_flutter/maps.dart';
import 'package:mapsforge_flutter/marker.dart';

import 'map-file-data.dart';
//-----------------------------------------------------------------------------<

/// The [StatefulWidget] displaying the interactive map page. This is a demo
/// implementation for using mapsforge's [MapviewWidget].
///
class MapViewPage2 extends StatefulWidget {
  final MapFileData mapFileData;

  final MapDataStore? mapFile;
  //--------------------------------------------------------------------------->
  //added renderThemeSrc
  final String? renderThemeSrc;

  const MapViewPage2(
      {Key? key,
      required this.mapFileData,
      required this.mapFile,
      this.renderThemeSrc})
      : super(key: key);

  //additionally pass mapFileData and renderThemeSrc
  @override
  MapViewPageState2 createState() => MapViewPageState2(
      mapFileData: mapFileData, renderThemeSrc: renderThemeSrc);
  //---------------------------------------------------------------------------<
}

/////////////////////////////////////////////////////////////////////////////

/// The [State] of the [MapViewPage] Widget.
class MapViewPageState2 extends State<MapViewPage2> {
  final DisplayModel displayModel = DisplayModel();

  //--------------------------------------------------------------------------->
  MapFileData mapFileData;
  final String? renderThemeSrc;

  MapViewPageState2({required this.mapFileData, this.renderThemeSrc});

  ViewModel? viewModel;
  List<IMarkerDataStore>? listMarkerDataStore; //list for storing marker
  MapMarkerDatastore? mapMarkerDataStore;
  MapMarkerDatastoreRoutingPath? mapMarkerDatastoreRoutingPath;
  MapMarkerDatastoreRoutingPath? mapMarkerDatastoreRoutingPosition;

  DirectionOverlay? directionOverlay;
  final directionOverlayKey = GlobalKey<DirectionOverlayState>();
  //---------------------------------------------------------------------------<

  @override
  Widget build(BuildContext context) {
    //---------------------initialize tap stream listener---------------------->
    //_listenToTapEventStream();
    //-------------------------------------------------------------------------<
    return Scaffold(
      appBar: _buildHead(context) as PreferredSizeWidget,
      body: _buildMapViewBody(context),
    );
  }

  /// Constructs the [AppBar] of the [MapViewPage] page.
  Widget _buildHead(BuildContext context) {
    return AppBar(
      title: Text(widget.mapFileData.displayedName),
      //----------------add menu for the added functionalities----------------->
      actions: <Widget>[
        PopupMenuButton<String>(
          icon: const Icon(CupertinoIcons.search),
          offset: const Offset(0, 50),
          onSelected: (choice) => _handleMenuItemSelect(choice, context),
          itemBuilder: (BuildContext context) => [
            // const PopupMenuItem<String>(
            //   value: "marker",
            //   child: Text("Search in Map Data"),
            // ),
            const PopupMenuItem<String>(
                value: "ble", child: Text("Tag verbinden")),
            const PopupMenuItem<String>(
              value: "route",
              child: Text("Route 1"),
            ),
            const PopupMenuItem<String>(
              value: "overlay",
              child: Text("Enable Direction Overlay"),
            ),
            /*  //separation of online and offline
            const PopupMenuItem<String>(
              value: "online",
              child: Text("Online Marker"),
            ),
            const PopupMenuItem<String>(
              value: "offline",
              child: Text("Offline Marker"),
            ),*/
            // const PopupMenuDivider(),
            // const PopupMenuItem<String>(
            //   value: "analyse_view",
            //   child: Text("Analyse current view"),
            // ),
          ],
        ),
      ],
      //-----------------------------------------------------------------------<
    );
  }

  /// Constructs the body ([FlutterMapView]) of the [MapViewPage].
  Widget _buildMapViewBody(BuildContext context) {
    return Stack(fit: StackFit.expand, children: <Widget>[
      MapviewWidget(
          displayModel: displayModel,
          createMapModel: () async {
            /// instantiate the job renderer. This renderer is the core of the system and retrieves or renders the tile-bitmaps
            return widget.mapFileData.mapType == MAPTYPE.OFFLINE
                ? await _createOfflineMapModel()
                : await _createOnlineMapModel();
          },
          createViewModel: () async {
            return _createViewModel();
          }),
    ]);
  }

  ViewModel _createViewModel() {
    // in this demo we use the markers only for offline databases.
    //-------------------viewModel changed to global variable-------------------
    //-----------and therefore added null check to subsequent variables---------
    viewModel = ViewModel(
      displayModel: displayModel,
      contextMenuBuilder: //DebugContextMenuBuilder(datastore: widget.mapFile!),
          widget.mapFileData.mapType == MAPTYPE.OFFLINE
              ? MarkerdemoContextMenuBuilder()
              : const DefaultContextMenuBuilder(),
    );
    if (widget.mapFileData.indoorZoomOverlay)
      viewModel!.addOverlay(IndoorlevelZoomOverlay(viewModel!,
          indoorLevels: widget.mapFileData.indoorLevels));
    else
      viewModel!.addOverlay(ZoomOverlay(viewModel!));
    viewModel!.addOverlay(DistanceOverlay(viewModel!));

    //---------------added the overlay for the directions---------------
    DirectionOverlay directionOverlay =
        DirectionOverlay(key: directionOverlayKey);
    viewModel!.addOverlay(directionOverlay);

    viewModel!.addOverlay(
        PositionButton(viewModel!, mapMarkerDatastoreRoutingPosition));

    //viewModel.addOverlay(DemoOverlay(viewModel: viewModel));

    // set default position
    viewModel!.setMapViewPosition(widget.mapFileData.initialPositionLat,
        widget.mapFileData.initialPositionLong);
    viewModel!.setZoomLevel(widget.mapFileData.initialZoomLevel);
    return viewModel!;
  }

  Future<MapModel> _createOfflineMapModel() async {
    /// For the offline-maps we need a cache for all the tiny symbols in the map
    final SymbolCache symbolCache;
    if (kIsWeb) {
      symbolCache = MemorySymbolCache(bundle: rootBundle);
    } else {
      symbolCache =
          FileSymbolCache(rootBundle, widget.mapFileData.relativePathPrefix);
    }

    //------------------------------------------------------------------------->
    /// Prepare the Themebuilder. This instructs the renderer how to draw the images
    RenderTheme renderTheme;
    if (renderThemeSrc != null) {
      renderTheme =
          await RenderThemeBuilder.create(displayModel, renderThemeSrc!);
      //renderTheme = await RenderThemeBuilder.create(displayModel, widget.mapFileData.theme);
      print("renderThemeSrc: ${renderThemeSrc}"); // test purposes
    } else {
      renderTheme = await RenderThemeBuilder.create(
          displayModel, widget.mapFileData.theme);
      print(
          "widget.mapFileData.theme: ${widget.mapFileData.theme}"); // test purposes
    }
    //-------------------------------------------------------------------------<

    /// instantiate the job renderer. This renderer is the core of the system and retrieves or renders the tile-bitmaps
    final JobRenderer jobRenderer = MapDataStoreRenderer(
        widget.mapFile!, renderTheme, symbolCache, true,
        useIsolate: false);

    /// and now it is similar to online rendering.

    /// provide the cache for the tile-bitmaps. In Web-mode we use an in-memory-cache
    final TileBitmapCache bitmapCache;
    if (kIsWeb) {
      bitmapCache = MemoryTileBitmapCache.create();
    } else {
      bitmapCache =
          await FileTileBitmapCache.create(jobRenderer.getRenderKey());
    }

    /// Now we can glue together and instantiate the mapModel and the viewModel. The former holds the
    /// properties for the map and the latter holds the properties for viewing the map
    MapModel mapModel = MapModel(
      displayModel: displayModel,
      renderer: jobRenderer,
      tileBitmapCache: bitmapCache,
      symbolCache: symbolCache,
    );
    mapModel.markerDataStores
        .add(MarkerdemoDatastore(symbolCache: symbolCache));
    //mapModel.markerDataStores.add(DebugDatastore(symbolCache: symbolCache));

    //------------------------initialize MarkerDataStore----------------------->
    listMarkerDataStore = mapModel.markerDataStores;

    mapMarkerDataStore = MapMarkerDatastore(symbolCache: symbolCache);
    listMarkerDataStore?.add(mapMarkerDataStore!);

    mapMarkerDatastoreRoutingPath =
        MapMarkerDatastoreRoutingPath(symbolCache: symbolCache);
    listMarkerDataStore?.add(mapMarkerDatastoreRoutingPath!);

    mapMarkerDatastoreRoutingPosition =
        MapMarkerDatastoreRoutingPath(symbolCache: symbolCache);
    listMarkerDataStore?.add(mapMarkerDatastoreRoutingPosition!);
    //-------------------------------------------------------------------------<

    return mapModel;
  }

  Future<MapModel> _createOnlineMapModel() async {
    /// instantiate the job renderer. This renderer is the core of the system and retrieves or renders the tile-bitmaps
    JobRenderer jobRenderer = widget.mapFileData.mapType == MAPTYPE.OSM
        ? MapOnlineRendererWeb()
        : ArcGisOnlineRenderer();

    /// provide the cache for the tile-bitmaps. In Web-mode we use an in-memory-cache
    final TileBitmapCache bitmapCache;
    if (kIsWeb) {
      bitmapCache = MemoryTileBitmapCache.create();
    } else {
      bitmapCache =
          await FileTileBitmapCache.create(jobRenderer.getRenderKey());
    }

    /// Now we can glue together and instantiate the mapModel and the viewModel. The former holds the
    /// properties for the map and the latter holds the properties for viewing the map
    MapModel mapModel = MapModel(
      displayModel: displayModel,
      renderer: jobRenderer,
      tileBitmapCache: bitmapCache,
    );

    return mapModel;
  }

  //----------------------------added functionality-----------------------------

  //---------------execute the selected action of the popup menu--------------->
  void _handleMenuItemSelect(String value, BuildContext context) {
    switch (value) {
      case 'ble':
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Dialog(child: BleWidget(mapMarkerDatastoreRoutingPath));
            });
        break;
      case 'analyse_view':
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) =>
                ViewPage(widget.mapFileData, viewModel!),
          ),
        );
        break;
      case "marker":
        showDialog(
          context: context,
          builder: (BuildContext context) => SearchWidget(
              viewModel!, mapFileData, mapMarkerDataStore!, context),
        );
        break;
      case "route":
        List<ILatLong> latLongList = [
          const LatLong(50.84181819363427, 12.92613722446498),
          const LatLong(50.84179910547996, 12.926216224497695),
          const LatLong(50.84181203616599, 12.926225000493186),
          const LatLong(50.841661722249434, 12.926851614649188),
          const LatLong(50.84160258833109, 12.926821380862513),
          const LatLong(50.84156256458778, 12.926987192528273),
          const LatLong(50.84142923249634, 12.926916486293564),
          const LatLong(50.84137104390358, 12.92688625250689),
          const LatLong(50.841153702283655, 12.927797573738609),
          const LatLong(50.840863064755816, 12.927614210081831),
          const LatLong(50.84073481069477, 12.928156457305233),
          const LatLong(50.839961152807014, 12.927712771261968),
          const LatLong(50.839924778302844, 12.92773328073129),
          const LatLong(50.83982933839709, 12.927682205164835),
          const LatLong(50.83971665179593, 12.927546643358973),
          const LatLong(50.839214704894474, 12.927258932151435),
          const LatLong(50.839178373792834, 12.9274227828805)
        ];
        ILatLong latLong = const LatLong(50.84181819363427, 12.92613722446498);
        mapMarkerDatastoreRoutingPath
            ?.createRouteMarkerFromLatLongs(latLongList);
        //mapMarkerDatastoreRoutingPath?.createCircleMarkerFromLatLong(latLong);
        break;
      case "overlay":
        directionOverlayKey.currentState?.updateVisibility();
        break;
      /*
      //the following is not used at the moment
      case "online":
        showDialog(
          context: context,
          builder: (BuildContext context) => _buildPopupDialog(context, false),
        );
        break;
      case "offline":
        showDialog(
          context: context,
          builder: (BuildContext context) => _buildPopupDialog(context, true),
        );
        break;*/
    }
  }
  //---------------------------------------------------------------------------<

  //--------------------------Listen to Tap on the Map------------------------->
  //TODO: after reload another stream listener is instantiated
  // Future _listenToTapEventStream() async {
  //   Stream<TapEvent> stream = viewModel!.observeTap;
  //
  //   MarkerDataStore markerDataStore = new MarkerDataStore();
  //   listMarkerDataStore!.add(markerDataStore);
  //   //print("TapListener"); //for debugging
  //   await for (TapEvent event in stream) {
  //     //event.longitude, event.latitude, event.x, event.y
  //     print("Tap ${event.latitude}, ${event.longitude}"); //for debugging
  //
  //     //here for example it is possible to make an overpass request around the
  //     // Tap with getEntitiesInArea
  //
  //     /*MapElements? _mapElements =
  //         await OverpassManage(overpassApi: _overpassApi).getElementsInArea(
  //             longitude: event.longitude,
  //             latitude: event.latitude,
  //             radius: 4);*/
  //   }
  //  return;
  //}
  //---------------------------------------------------------------------------<

}
