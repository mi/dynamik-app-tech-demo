import 'dart:io' show Platform;

import 'package:easylocate_flutter_sdk/easylocate_device_api.dart';
import 'package:easylocate_flutter_sdk/easylocate_sdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mapsforge_flutter/core.dart';
import 'package:permission_handler/permission_handler.dart';

import '../marker/marker_datastore_routing_path.dart';

class BleWidget extends StatefulWidget {
  //needed for showing markers
  MapMarkerDatastoreRoutingPath? mapMarkerDataStore;

  BleWidget(this. mapMarkerDataStore, {Key? key})
      : super(key: key);

  @override
  _BleWidgetState createState() {
    return _BleWidgetState(mapMarkerDataStore);
  }
}

class _BleWidgetState extends State<BleWidget> {
  final _easySdk = EasyLocateSdk();

  // connection buttons with enabled state
  Map<TextButton, bool> _connButtons = {};

  // command buttons with enabled state
  Map<TextButton, bool> _commandButtons = {};

  // connection vars
  late String _selectedDevice = "";
  late List<BleDevice> _bleDevices = [];
  late List<DropdownMenuItem<String>> _availablePorts = [];

  // command vars
  var _togglePosition = [20.0, 4.4, 104.1];
  var _toggleSiteId = 0;
  var _togglePanId = 0;
  var _positioningInterval = 1;
  var _motionCheckInterval = 1;

  // startPositioning, stopPositioning test api
  late EasyLocateApi _startStopPositioningTestApi;

  // console vars
  String _consoleMsg = "";

  // needed for showing markers
  MapMarkerDatastoreRoutingPath? _mapMarkerDatastore;
  _BleWidgetState(this._mapMarkerDatastore);

  /// UI Aufbau
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(
        children: [
          // Container(
          //     width: 120.0,
          //     margin: const EdgeInsets.only(
          //         left: 10.0, right: 5.0, top: 20.0, bottom: 20.0),
          //     child: _connButtons.keys.elementAt(0)),
          Container(
              width: 120.0,
              margin: const EdgeInsets.only(
                  left: 10.0, right: 5.0, top: 20.0, bottom: 20.0),
              child: _connButtons.keys.elementAt(1)),
        ],
      ),
      Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
            width: 100.0,
            margin: const EdgeInsets.only(left: 10.0, right: 0.0),
            child: const Text(
              "Device",
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 16),
            )),
        Container(
          width: 150.0,
          child: DropdownButton(
            isExpanded: true,
            style: const TextStyle(color: Colors.black, fontSize: 16),
            value: _selectedDevice,
            items: _availablePorts,
            onChanged: (String? newValue) {
              setState(() {
                _selectedDevice = newValue!;
              });
            },
          ),
        )
        //   Container(
        //       width: 100.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(0)),
        //   Container(
        //       width: 100.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(1)),
        //   Container(
        //       width: 100.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(2)),
        //   Container(
        //       width: 140.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(3)),
        //   Container(
        //       width: 120.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(4)),
        //   Container(
        //       width: 120.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(5)),
        //   Container(
        //       width: 100.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(6)),
        //   Container(
        //       width: 100.0,
        //       margin: const EdgeInsets.only(
        //           left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        //       child: _commandButtons.keys.elementAt(7)),
      ]),
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //     Container(
          //         width: 120.0,
          //         margin: const EdgeInsets.only(
          //             left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
          //         child: _commandButtons.keys.elementAt(8)),
          //     Container(
          //         width: 140.0,
          //         margin: const EdgeInsets.only(
          //             left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
          //         child: _commandButtons.keys.elementAt(9)),
          Container(
              width: 100.0,
              margin: const EdgeInsets.only(
                  left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
              child: _commandButtons.keys.elementAt(10)),
          Container(
              width: 100.0,
              margin: const EdgeInsets.only(
                  left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
              child: _commandButtons.keys.elementAt(11))
        ],
      ),
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              margin: const EdgeInsets.only(left: 10.0, right: 0.0, top: 20),
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              decoration: const BoxDecoration(
                  color: Color(0xffe1e3e2),
                  borderRadius: BorderRadius.all(Radius.circular(3))),
              child: Text(
                _consoleMsg,
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(fontSize: 16),
              )),
        ],
      ),
    ]);
  }

  //// functionality methods ////

  @override
  void initState() {
    super.initState();
    collectConnectionButtons();
    collectCommandButtons();
    setupPlatformSupport();
  }

  void reset() {
    setState(() {
      DateTime now = DateTime.now();
      _consoleMsg = "Request GUI reset at ${now.hour}:${now.minute} ...\n";
      _connButtons = {};
      _commandButtons = {};
      _availablePorts = [];
      _selectedDevice = "";
    });
    collectConnectionButtons();
    collectCommandButtons();
  }

  void log(String msg) {
    setState(() {
      _consoleMsg = msg;
    });
  }

  void setupPlatformSupport() {
    if (Platform.isAndroid) {
      initAndroidSupport();
    } else if (Platform.isIOS) {
      initIOSSupport();
    } else if (Platform.isWindows) {
      initWindowsSupport();
    } else if (Platform.isMacOS) {
      initMacOsSupport();
    } else if (Platform.isLinux) {
      initLinuxSupport();
    }
  }

  void initAndroidSupport() async {
    await askBlePermissions();
  }

  void initIOSSupport() async {
    await askBlePermissions();
  }

  void initWindowsSupport() {
    // TODO implement
  }

  void initMacOsSupport() {
    // TODO implement
  }

  void initLinuxSupport() {
    // TODO implement
  }

  Future<void> askBlePermissions() async {
    var blScan = await Permission.bluetoothScan.request().isGranted;
    var blCon = await Permission.bluetoothConnect.request().isGranted;
    var blAd = await Permission.bluetoothAdvertise.request().isGranted;

    if (!blScan) {
      if (kDebugMode) {
        print("BLUETOOTH_SCAN PERMISSION DENIED!");
      }
    }
    if (!blCon) {
      if (kDebugMode) {
        print("BLUETOOTH_CONNECT PERMISSION DENIED!");
      }
    }
    if (!blAd) {
      if (kDebugMode) {
        print("BLUETOOTH_ADVERTISE PERMISSION DENIED!");
      }
    }
  }

  bool setupUsbConnection() {
    // currently allowed: windows, macos, android, linux
    var now = DateTime.now();
    var msg = "Request USB connection ${now.hour}:${now.minute} ...";
    if (!Platform.isWindows &&
        !Platform.isMacOS &&
        !Platform.isLinux &&
        !Platform.isAndroid) {
      log("$msg \n\nPlatform does not support usb connection.");
      return false;
    }
    log("$msg \n\n");

    setupPlatformSupport();

    // search for ports
    _availablePorts = [];
    setState(() {
      for (var port in _easySdk.listUsbDevices()) {
        _availablePorts.add(
            DropdownMenuItem(value: port.portName, child: Text(port.portName)));
      }
    });
    if (_availablePorts.isNotEmpty) {
      _selectedDevice = _availablePorts.first.value!;
    }
    return true;
  }

  Future<bool> setupBleConnection() async {
    var now = DateTime.now();
    var msg = "Request Bluetooth connection ${now.hour}:${now.minute} ...";
    if (!Platform.isMacOS && !Platform.isIOS && !Platform.isAndroid) {
      log("$msg \n\nPlatform does not support bluetooth connection.");
      return false;
    }
    log("$msg \n\n");

    // scan for devices
    var myScanListener = MyBleScanListener();
    var scanTimeout = const Duration(seconds: 2);
    _easySdk.startBleScan(myScanListener, scanTimeout);
    await Future.delayed(scanTimeout); // wait for scanning
    _easySdk.stopBleScan();
    _bleDevices = myScanListener.listBleDevices();

    if (_bleDevices.isEmpty) {
      log("No bluetooth device found.");
      return false;
    }

    _availablePorts = [];
    setState(() {
      for (var device in _bleDevices) {
        var ddMenuItems =
            _availablePorts.where((item) => item.value == device.name);
        if (ddMenuItems.isEmpty) {
          _availablePorts.add(
              DropdownMenuItem(value: device.name, child: Text(device.name)));
        }
      }
    });
    if (_availablePorts.isNotEmpty) {
      _selectedDevice = _availablePorts.first.value!;
    }
    return true;
  }

  void collectConnectionButtons() {
    var usb = TextButton(
      onPressed: () => onUsbConnectionPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            return (_connButtons.isEmpty || _connButtons.values.elementAt(0))
                ? Colors.blue
                : Colors.grey;
          },
        ),
      ),
      child: const Text("USB connection"),
    );

    var ble = TextButton(
      onPressed: () => onBleConnectionPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            return (_connButtons.isEmpty || _connButtons.values.elementAt(1))
                ? Colors.blue
                : Colors.grey;
          },
        ),
      ),
      child: const Text("BLE connection"),
    );

    _connButtons[usb] = true;
    _connButtons[ble] = true;
  }

  void onUsbConnectionPressed() {
    if (!_connButtons.values.elementAt(0)) {
      return;
    }
    if (!setupUsbConnection()) {
      return;
    }
    setState(() {
      // disable bluetooth connection button
      _connButtons.update(_connButtons.keys.elementAt(1), (value) => false);
      // enable command buttons
      _commandButtons.forEach(
          (key, value) => _commandButtons.update(key, (value) => true));
    });
  }

  void onBleConnectionPressed() async {
    if (!_connButtons.values.elementAt(1)) {
      return;
    }
    if (!(await setupBleConnection())) {
      return;
    }
    setState(() {
      // disable usb connection button
      _connButtons.update(_connButtons.keys.elementAt(0), (value) => false);
      // enable command buttons
      _commandButtons.forEach(
          (key, value) => _commandButtons.update(key, (value) => true));
    });
  }

  void collectCommandButtons() {
    var cmdLoop = TextButton(
      onPressed: () => onCmdLoopPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(0))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: const Text("Cmd Loop"),
    );

    var showMe = TextButton(
      onPressed: () => onShowMePressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(1))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: const Text("Show Me"),
    );

    var getStatus = TextButton(
      onPressed: () => onGetStatusPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(2))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: const Text("Get Status"),
    );

    var setSatletPos = TextButton(
      onPressed: () => onSetSatletPositionPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(3))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: Text(
          "SetSatletPosition (${_togglePosition[0]}, ${_togglePosition[1]}, ${_togglePosition[2]})"),
    );

    var setSiteId = TextButton(
      onPressed: () => onSetSiteIdPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(4))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: Text("SetSiteId (0x${_toggleSiteId.toRadixString(16)})"),
    );

    var setPanId = TextButton(
      onPressed: () => onSetPanIdPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(5))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: Text("SetPanId (0x${_togglePanId.toRadixString(16)})"),
    );

    var flashConfig = TextButton(
      onPressed: () => onFlashConfigPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(6))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: const Text("Flash Config"),
    );

    var setPosInterval = TextButton(
      onPressed: () => onSetPosIntervalPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(7))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: Text("Set Positioning Interval to $_positioningInterval"),
    );

    var setMotionCheckInterval = TextButton(
      onPressed: () => onSetMotionCheckIntervalPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(8))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: Text("Set Motion Check Interval to $_motionCheckInterval"),
    );

    var startPositioning = TextButton(
      onPressed: () => onStartPositioningPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(9))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: const Text("Start Positioning"),
    );

    var stopPositioning = TextButton(
      onPressed: () => onStopPositioningPressed(),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          return (_commandButtons.isEmpty ||
                  !_commandButtons.values.elementAt(10))
              ? Colors.grey
              : Colors.blue;
        }),
      ),
      child: const Text("Stop Positioning"),
    );

    var getAddress = TextButton(
        onPressed: () => onGetAddressPressed(),
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            return (_commandButtons.isEmpty ||
                    !_commandButtons.values.elementAt(11))
                ? Colors.grey
                : Colors.blue;
          }),
        ),
        child: const Text("Get Address"));

    _commandButtons[cmdLoop] = false;
    _commandButtons[showMe] = false;
    _commandButtons[getStatus] = false;
    _commandButtons[setSatletPos] = false;
    _commandButtons[setSiteId] = false;
    _commandButtons[setPanId] = false;
    _commandButtons[flashConfig] = false;
    _commandButtons[getAddress] = false;
    _commandButtons[setPosInterval] = false;
    _commandButtons[setMotionCheckInterval] = false;
    _commandButtons[startPositioning] = false;
    _commandButtons[stopPositioning] = false;
  }

  Future<EasyLocateApi> connect() async {
    if (_connButtons.values.elementAt(0)) {
      // usb connection
      return _easySdk.connectUsb(UsbDevice(_selectedDevice));
    } else {
      // ble connection
      if (_bleDevices.isEmpty) {
        log("No bluetooth devices found");
      }
      var bleDevice = _bleDevices.first;
      for (var device in _bleDevices) {
        if (device.name == _selectedDevice) {
          bleDevice = device;
          break;
        }
      }
      return await _easySdk.connectBle(bleDevice);
    }
  }

  void onCmdLoopPressed() async {
    if (!_commandButtons.values.elementAt(0)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var result = await easyApi.loopCommand(2);
      DateTime now = DateTime.now();
      log("Request command loop at ${now.hour}:${now.minute} ...\n\nCommand loop result: $result");
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onShowMePressed() async {
    if (!_commandButtons.values.elementAt(1)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      await easyApi.showMe();
      DateTime now = DateTime.now();
      log("Request Show Me at ${now.hour}:${now.minute} ...\n");
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onGetStatusPressed() async {
    if (!_commandButtons.values.elementAt(2)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var status = await easyApi.getStatus();
      DateTime now = DateTime.now();
      log("Request status at ${now.hour}:${now.minute} ...\n\n$status");
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onSetSatletPositionPressed() async {
    if (!_commandButtons.values.elementAt(3)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var success = await easyApi.setSatletPosition(
          _togglePosition[0], _togglePosition[1], _togglePosition[2]);
      var status = await easyApi.getStatus();
      DateTime now = DateTime.now();
      success
          ? log(
              "Request set satlet position at ${now.hour}:${now.minute} ...\n\nSuccessfully set position:\n$status")
          : log(
              "Request set satlet position at ${now.hour}:${now.minute} ...\n\nFailed set position\n");
      setState(() {
        if (_togglePosition[0] != 1) {
          _togglePosition = [1.0, 1.0, 1.0];
        } else {
          _togglePosition = [20.0, 4.4, 104.1];
        }
      });
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onSetSiteIdPressed() async {
    if (!_commandButtons.values.elementAt(4)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;

    try {
      easyApi = await connect();
      var success = await easyApi.setSiteId(_toggleSiteId);
      var status = await easyApi.getStatus();
      DateTime now = DateTime.now();
      success
          ? log(
              "Request set site id at ${now.hour}:${now.minute} ...\n\nSuccessfully set site id:\n$status")
          : log(
              "Request set site id at ${now.hour}:${now.minute} ...\n\nFailed set site id\n");
      setState(() {
        if (_toggleSiteId == 0x0) {
          _toggleSiteId = 0x18;
        } else {
          _toggleSiteId = 0x0;
        }
      });
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onSetPanIdPressed() async {
    if (!_commandButtons.values.elementAt(5)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var success = await easyApi.setPanId(_togglePanId);
      var status = await easyApi.getStatus();
      DateTime now = DateTime.now();
      success
          ? log(
              "Request set pan id at ${now.hour}:${now.minute} ...\n\nSuccessfully set pan id:\n$status")
          : log(
              "Request set pan id at ${now.hour}:${now.minute} ...\n\nFailed set pan id\n");
      setState(() {
        if (_togglePanId == 0x0) {
          _togglePanId = 143;
        } else {
          _togglePanId = 0x0;
        }
      });
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onFlashConfigPressed() async {
    if (!_commandButtons.values.elementAt(6)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var success = await easyApi.flashConfig();
      DateTime now = DateTime.now();
      success
          ? log(
              "Request flash config ${now.hour}:${now.minute} ...\n\nSuccessfully flashed configuration.")
          : log(
              "Request flash config ${now.hour}:${now.minute} ...\n\nFailed flashing configuration.");
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onSetPosIntervalPressed() async {
    if (!_commandButtons.values.elementAt(7)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var success = await easyApi.setPositioningInterval(_positioningInterval);
      DateTime now = DateTime.now();
      success
          ? log(
              "Request set positioning interval ${now.hour}:${now.minute} ...\n\nSuccessfully set positioning interval.")
          : log(
              "Request set positioning interval ${now.hour}:${now.minute} ...\n\nFailed setting positioning interval.");
      setState(() {
        if (_positioningInterval == 1) {
          _positioningInterval = 2;
        } else {
          _positioningInterval = 1;
        }
      });
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onSetMotionCheckIntervalPressed() async {
    if (!_commandButtons.values.elementAt(8)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var success = await easyApi.setMotionCheckInterval(_positioningInterval);
      DateTime now = DateTime.now();
      success
          ? log(
              "Request set motion check interval ${now.hour}:${now.minute} ...\n\nSuccessfully set motion check interval.")
          : log(
              "Request set motion check interval ${now.hour}:${now.minute} ...\n\nFailed setting motion check interval.");
      setState(() {
        if (_motionCheckInterval == 1) {
          _motionCheckInterval = 2;
        } else {
          _motionCheckInterval = 1;
        }
      });
    } on Exception catch (e) {
      print(e);
    } finally {
      easyApi.disconnect();
    }
  }

  void onStartPositioningPressed() async {
    if (!_commandButtons.values.elementAt(9)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    try {
      _startStopPositioningTestApi = await connect();
      _startStopPositioningTestApi.setWgs84Reference(
          50.8416615, 12.9260784, 20.0);
      await _startStopPositioningTestApi
          .startPositioning(MyTagPositionListener(_mapMarkerDatastore));
      DateTime now = DateTime.now();
      log("Request start positioning ${now.hour}:${now.minute}");
    } on Exception catch (e) {
      print(e);
    }
  }

  void onStopPositioningPressed() async {
    if (!_commandButtons.values.elementAt(10)) {
      // button disabled
      return;
    }
    try {
      await _startStopPositioningTestApi.stopPositioning();
      _startStopPositioningTestApi.disconnect();
      DateTime now = DateTime.now();
      log("Request stop positioning ${now.hour}:${now.minute}");
    } on Exception catch (e) {
      print(e);
    }
  }

  void onGetAddressPressed() async {
    if (!_commandButtons.values.elementAt(11)) {
      // button disabled
      return;
    }
    if (_selectedDevice == "") {
      log("No device connected...\n");
      return;
    }
    late EasyLocateApi easyApi;
    try {
      easyApi = await connect();
      var address = await easyApi.getAddress();
      DateTime now = DateTime.now();
      log("Request device address at ${now.hour}:${now.minute} ...\n\nDevice address: 0x${address.toRadixString(16)}");
    } on Exception catch (e) {
      if (kDebugMode) {
        print(e);
      }
    } finally {
      easyApi.disconnect();
    }
  }
}

class MyBleScanListener extends BleScanListener {
  /// List of scanned ble devices
  List<BleDevice> _bleDevice = [];

  List<BleDevice> listBleDevices() {
    return _bleDevice;
  }

  @override
  void onDeviceApproached(BleDevice bleDevice) {
    if (kDebugMode) {
      print("Closest device found:");
    }
    if (kDebugMode) {
      print(
          "Name: ${bleDevice.name}, Address: ${bleDevice.address}, RSSI: ${bleDevice.rssi}, TX (not supported right now): ${bleDevice.tx}");
    }
  }

  @override
  void onScanResults(List<BleDevice> bleDevices) {
    print("Device found:");
    bleDevices.forEach((device) {
      if (kDebugMode) {
        print(
            "Name: ${device.name}, Address: ${device.address}, RSSI: ${device.rssi}, TX (not supported right now): ${device.tx}");
      }
    });
    _bleDevice = bleDevices;
  }
}

class MyTagPositionListener extends TagPositionListener {
  MapMarkerDatastoreRoutingPath? mapMarkerDatastore;

  MyTagPositionListener(this.mapMarkerDatastore);

  @override
  void onLocalPosition(LocalPosition localPosition) {
    if (kDebugMode) {
      print(
          "Local Position: X: ${localPosition.x}, Y: ${localPosition.y}, Z: ${localPosition.z}");
    }
  }

  @override
  void onWgs84Position(Wgs84Position wgs84position) {
    mapMarkerDatastore?.clearMarkers();
    mapMarkerDatastore?.createCircleMarkerFromLatLong(
        LatLong(wgs84position.lat, wgs84position.lon));

    if (kDebugMode) {
      print(
          "WGS84: Lat: ${wgs84position.lat}, Lon: ${wgs84position.lon}, Z: ${wgs84position.z}");
    }
  }
}
