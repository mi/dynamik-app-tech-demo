import 'package:flutter/material.dart';
import 'package:mapsforge_flutter/core.dart';

//----------------------------------------------------------------------------->
import 'package:mapsforge_example/marker/marker_tags.dart';
import 'package:mapsforge_example/mapElements/map_elements.dart';

import 'package:mapsforge_example/overpass/overpass_manage.dart';
import 'package:mapsforge_example/overpass/overpass_api.dart';

import 'package:mapsforge_example/marker/marker_datastore.dart';
import 'package:mapsforge_example/offlineData/offlineData_manage.dart';

import '../map-file-data.dart';
//-----------------------------------------------------------------------------<

class SearchWidget extends StatefulWidget {
  final ViewModel viewModel;
  late MapMarkerDatastore mapMarkerDataStore;
  late BuildContext context;
  late MapFileData mapFileData;

  SearchWidget(
      this.viewModel, this.mapFileData, this.mapMarkerDataStore, this.context);

  @override
  State<StatefulWidget> createState() =>
      _SearchWidgetState(viewModel, mapFileData, mapMarkerDataStore, context);
}

/////////////////////////////////////////////////////////////////////////////

class _SearchWidgetState extends State<SearchWidget>
    with SingleTickerProviderStateMixin {
  @override
  late BuildContext context;

  final double toolbarSpacing = 15;

  final OverpassApi _overpassApi = new OverpassApi();
  MapMarkerDatastore mapMarkerDataStore;
  MapFileData mapFileData;
  ViewModel viewModel;

  _SearchWidgetState(
      this.viewModel, this.mapFileData, this.mapMarkerDataStore, this.context);

  late AnimationController _fadeAnimationController;

  @override
  SearchWidget get widget => super.widget;

  @override
  void initState() {
    _fadeAnimationController = AnimationController(
      duration: const Duration(milliseconds: 1000),
      //value: 1,
      vsync: this,
      //lowerBound: 0,
      //upperBound: 1,
    );

    super.initState();
  }

  @override
  void dispose() {
    _fadeAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _fadeAnimationController.forward();
    return
        //------------------------mixed online and offline search-------------------->
        // and visualization of the results with marker
        new AlertDialog(
      title: const Text('Search'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const MyStatefulWidgetCheckbox(),
        ],
      ),
      actions: <Widget>[
        new ElevatedButton(
          child: const Text('Delete Markers'),
          onPressed: () {
            Navigator.of(context).pop();
            MapMarkerDatastore.clearMarkerDataStore(mapMarkerDataStore);
          },
        ),

        //TODO: error message for timeout
        new ElevatedButton(
            child: const Text('Search'),
            onPressed: () async {
              List<String>? response;
              bool isOffline = true;

              selectedCheckbox.forEach((key, value) async {
                if (value == true) {
                  //the offline map data doesn't contain all data so only
                  // selected search queries can be used
                  if (key == 'footway' ||
                      key == 'steps' ||
                      key == 'floor_asphalt' ||
                      key == 'floor_plaster' ||
                      key == 'floor_paving_stones' ||
                      key == 'wheelchair_yes') isOffline = false;
                }
              });
              if (!isOffline)
                response = await _onLoadingOnline();
              else
                response = await _onLoadingOffline();
              //Navigator.of(context).pop();
              //shows the MapElements
              if (response != null) {
                Navigator.of(context).pop();
                await showModalBottomSheet(
                  isScrollControlled: true,
                  isDismissible: true,
                  context: context,
                  builder: (context) => DraggableScrollableSheet(
                    initialChildSize: 0.4,
                    minChildSize: 0.2,
                    maxChildSize: 0.75,
                    expand: false,
                    builder: (_, controller) => Column(
                      children: [
                        Icon(
                          Icons.remove,
                          color: Colors.grey[600],
                        ),
                        Expanded(
                          child: ListView.builder(
                            controller: controller,
                            itemCount: response?.length,
                            itemBuilder: (_, index) {
                              return Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: Text('${response![index]}'),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }
            }),
      ],
    );
    //---------------------------------------------------------------------------<
  }

  //--------------online search with the help of the overpass API-------------->
  Future<List<String>?> _onLoadingOnline() async {
    // ignore: unawaited_futures
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          alignment: Alignment.center,
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(30),
                child: const CircularProgressIndicator(),
              ),
              Container(
                padding: const EdgeInsets.all(30),
                child: const Text("Loading ..."),
              )
            ],
          ),
        );
      },
    );

    Map<String, dynamic>? _filter;
    List<String> identifier = List.empty(growable: true);
    selectedCheckbox.forEach((key, value) {
      if (value == true) {
        identifier.add(key);
      }
    });

    if (identifier != {}) {
      _filter = MarkerTags().getTagsFromTagList(identifier);

      try {
        MapElements? _mapElements = await _overpassRequest(_filter);

        Navigator.of(context).pop();

        await mapMarkerDataStore.createMarkerFromMapElements(_mapElements);
        return MapElementsToStringList(_mapElements);
      } catch (exception) {
        Navigator.of(context).pop();

        await showDialog(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context) {
            return Dialog(
                alignment: Alignment.center,
                child: Container(
                  padding: const EdgeInsets.all(30),
                  child: const Text("Connecting to overpass server failed. "
                      "Please check your internet connection and try again. "
                      "If you don't have an internet connection, then do not use search terms that are tagged with '(online)'."),
                ));
          },
        );
        return null;
      }
    }

    return null;
  }
//---------------------------------------------------------------------------<

//---------------offline search in the map data from mapsforge--------------->
  Future<List<String>?> _onLoadingOffline() async {
    // ignore: unawaited_futures
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          alignment: Alignment.center,
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(30),
                child: const CircularProgressIndicator(),
              ),
              Container(
                padding: const EdgeInsets.all(30),
                child: const Text("Loading ..."),
              )
            ],
          ),
        );
      },
    );

    Map<String, dynamic>? _filter;
    List<String> identifier = List.empty(growable: true);
    selectedCheckbox.forEach((key, value) {
      if (value == true) {
        identifier.add(key);
      }
    });

    if (identifier != {}) {
      _filter = MarkerTags().getTagsFromTagList(identifier);
      OfflineData offlineData = new OfflineData(mapFileData, viewModel);
      print("$_filter");

      try {
        MapElements? _mapElements =
            await offlineData.getElementsInBoundingBox(_filter);

        Navigator.of(context).pop();

        await mapMarkerDataStore.createMarkerFromMapElements(_mapElements);
        return MapElementsToStringList(_mapElements);
      }
      //TODO: print error in app
      catch (exception) {
        Navigator.of(context).pop();
        return null;
      }
    }

    return null;
  }
//---------------------------------------------------------------------------<

//------------------------------Overpass Request----------------------------->
  Future<MapElements?> _overpassRequest(Map<String, dynamic>? filter) async {
    MapViewPosition? mapViewPosition = viewModel.mapViewPosition;
    BoundingBox boundingBox =
        mapViewPosition!.calculateBoundingBox(viewModel.viewDimension!);

    //with the "level" tag it is possible to search only in the current indoor level
    //var indoorLevel = mapViewPosition.indoorLevel;

    MapElements? _mapElements = await OverpassManage(overpassApi: _overpassApi)
        .getElementsInBoundingBox(
            minLatitude: boundingBox.minLatitude,
            minLongitude: boundingBox.minLongitude,
            maxLatitude: boundingBox.maxLatitude,
            maxLongitude: boundingBox.maxLongitude,
            filter: filter);

    //await mapMarkerDataStore?.createMarkerFromMapElements(_mapElements);
    //markerDataStore.getMarkersToPaint(boundingBox, mapViewPosition.zoomLevel); //just for testing

    return _mapElements;
  }
//---------------------------------------------------------------------------<

}

//--------------------Checkboxes for creating a search query------------------->
// TODO: outsource if possible, improve efficiency
Map<String, bool> selectedCheckbox = {
  //must be same as in MarkerTags
  'door': false,
  'tree': false,
  'bench': false,
  'toilet': false,
  //'highway_all': false,
  'footway': false,
  'steps': false,
  'tertiary': false,

  'wheelchair_yes': false,
  //'wheelchair_limited': false,
  'level_0': false,
  'level_1': false,
  'level_2': false,
  'level_3': false,
  'floor_asphalt': false,
  'floor_plaster': false,
  'floor_paving_stones': false,
}; // for (int i = 0 ; i < selectedCheckbox.length; i++ ){}

class MyStatefulWidgetCheckbox extends StatefulWidget {
  const MyStatefulWidgetCheckbox({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidgetCheckbox> createState() {
    return _MyStatefulWidgetCheckboxState();
  }
}

class _MyStatefulWidgetCheckboxState extends State<MyStatefulWidgetCheckbox> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 400,
        child: SingleChildScrollView(
            child: Column(children: <Widget>[
          LabeledCheckbox(
            label: "Door",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["door"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["door"] = !selectedCheckbox["door"]!;
                //_isSelected = newValue;
              });
            },
          ),
          LabeledCheckbox(
            label: "Tree",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["tree"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["tree"] = !selectedCheckbox["tree"]!;
                //_isSelected = newValue;
              });
            },
          ),
          LabeledCheckbox(
            label: "Bench",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["bench"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["bench"] = !selectedCheckbox["bench"]!;
                //_isSelected = newValue;
              });
            },
          ),
          LabeledCheckbox(
            label: "Toilet",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["toilet"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["toilet"] = !selectedCheckbox["toilet"]!;
                //_isSelected = newValue;
              });
            },
          ),
          /*LabeledCheckbox(
            label:
                "All Ways (Problem: blocking slots http://overpass-api.de/api/status)",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["highway_all"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["highway_all"] =
                    !selectedCheckbox["highway_all"]!;
                //_isSelected = newValue;
              });
            },
          ),*/
          LabeledCheckbox(
            label: "Tertiary",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["tertiary"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["tertiary"] = !selectedCheckbox["tertiary"]!;
                //_isSelected = newValue;
              });
            },
          ),
          LabeledCheckbox(
            label: "Footway (online)",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["footway"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["footway"] = !selectedCheckbox["footway"]!;
                //_isSelected = newValue;
              });
            },
          ),
          const PopupMenuDivider(),
          /*LabeledCheckbox(
            label: "Steps",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["steps"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["steps"] = !selectedCheckbox["steps"]!;
                //_isSelected = newValue;
              });
            },
          ),*/

          /*LabeledCheckbox(
            label: "Limited Wheelchair Accessibility",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["wheelchair_limited"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["wheelchair_limited"] =
                    !selectedCheckbox["wheelchair_limited"]!;
                //_isSelected = newValue;
              });
            },
          ),*/

          LabeledCheckbox(
            label: "Level 0",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["level_0"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["level_0"] = !selectedCheckbox["level_0"]!;
                //_isSelected = newValue;
              });
            },
          ),
          LabeledCheckbox(
            label: "Level 1",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["level_1"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["level_1"] = !selectedCheckbox["level_1"]!;
                //_isSelected = newValue;
              });
            },
          ),
          LabeledCheckbox(
            label: "Wheelchair Accessibility (online)",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["wheelchair_yes"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["wheelchair_yes"] =
                    !selectedCheckbox["wheelchair_yes"]!;
                //_isSelected = newValue;
              });
            },
          ),
          /*LabeledCheckbox(
            label: "Level 2",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["level_2"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["level_2"] = !selectedCheckbox["level_2"]!;
                //_isSelected = newValue;
              });
            },
          ),
          LabeledCheckbox(
            label: "Level 3",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["level_3"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["level_3"] = !selectedCheckbox["level_3"]!;
                //_isSelected = newValue;
              });
            },
          ),*/
          LabeledCheckbox(
            label: "Floor Material Asphalt (online)",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["floor_asphalt"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["floor_asphalt"] =
                    !selectedCheckbox["floor_asphalt"]!;
                //_isSelected = newValue;
              });
            },
          ),
          /*LabeledCheckbox(
            label: "Floor Material: Plaster",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["floor_plaster"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["floor_plaster"] =
                    !selectedCheckbox["floor_plaster"]!;
                //_isSelected = newValue;
              });
            },
          ),*/
          LabeledCheckbox(
            label: "Floor Material Paving Stones (online)",
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            value: selectedCheckbox["floor_paving_stones"]!,
            onChanged: (bool newValue) {
              setState(() {
                selectedCheckbox["floor_paving_stones"] =
                    !selectedCheckbox["floor_paving_stones"]!;
                //_isSelected = newValue;
              });
            },
          ),
        ])));
  }
}

class LabeledCheckbox extends StatelessWidget {
  const LabeledCheckbox({
    Key? key,
    required this.label,
    required this.padding,
    required this.value,
    required this.onChanged,
  }) : super(key: key);

  final String label;
  final EdgeInsets padding;
  final bool value;
  final ValueChanged<bool> onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onChanged(!value);
      },
      child: Padding(
        padding: padding,
        child: Row(
          children: <Widget>[
            Expanded(child: Text(label)),
            Checkbox(
              value: value,
              onChanged: (bool? newValue) {
                onChanged(newValue!);
              },
            ),
          ],
        ),
      ),
    );
  }
}
//-----------------------------------------------------------------------------<
