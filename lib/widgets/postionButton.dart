import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mapsforge_flutter/core.dart';

import '../marker/marker_datastore_routing_path.dart';

class PositionButton extends StatefulWidget {
  final ViewModel viewModel;

  MapMarkerDatastoreRoutingPath? mapMarkerDatastoreRoutingPath;

  PositionButton(this.viewModel, this.mapMarkerDatastoreRoutingPath);

  @override
  State<StatefulWidget> createState() {
    return _PositionButtonState(mapMarkerDatastoreRoutingPath);
  }
}

/////////////////////////////////////////////////////////////////////////////

class _PositionButtonState extends State<PositionButton>
    with SingleTickerProviderStateMixin {
  final double toolbarSpacing = 15;

  late AnimationController _fadeAnimationController;
  late CurvedAnimation _fadeAnimation;

  MapMarkerDatastoreRoutingPath? mapMarkerDatastoreRoutingPath;
  _PositionButtonState(this.mapMarkerDatastoreRoutingPath);

  List<ILatLong> latLongList = [
    const LatLong(50.84181819363427, 12.92613722446498),
    const LatLong(50.84179910547996, 12.926216224497695),
    const LatLong(50.84181203616599, 12.926225000493186),
    const LatLong(50.841661722249434, 12.926851614649188),
    const LatLong(50.84160258833109, 12.926821380862513),
    const LatLong(50.84156256458778, 12.926987192528273),
    const LatLong(50.84142923249634, 12.926916486293564),
    const LatLong(50.84137104390358, 12.92688625250689),
    const LatLong(50.841153702283655, 12.927797573738609),
    const LatLong(50.840863064755816, 12.927614210081831),
    const LatLong(50.84073481069477, 12.928156457305233),
    const LatLong(50.839961152807014, 12.927712771261968),
    const LatLong(50.839924778302844, 12.92773328073129),
    const LatLong(50.83982933839709, 12.927682205164835),
    const LatLong(50.83971665179593, 12.927546643358973),
    const LatLong(50.839214704894474, 12.927258932151435),
    const LatLong(50.839178373792834, 12.9274227828805)
  ];
  int i = 0;

  @override
  PositionButton get widget => super.widget;

  @override
  void initState() {
    super.initState();

    _fadeAnimationController = AnimationController(
      duration: const Duration(milliseconds: 1000),
      //value: 1,
      vsync: this,
      //lowerBound: 0,
      //upperBound: 1,
    );
    _fadeAnimation = CurvedAnimation(
      parent: _fadeAnimationController,
      curve: Curves.easeIn,
    );
  }

  @override
  void dispose() {
    _fadeAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _fadeAnimationController.forward();
    return Positioned(
      bottom: 60,
      //right: toolbarSpacing,
      //top: toolbarSpacing,
      left: toolbarSpacing,
      child: FadeTransition(
        opacity: _fadeAnimationController,
        child: RawMaterialButton(
          onPressed: () {
            mapMarkerDatastoreRoutingPath?.clearMarkers();
            if (i >= (latLongList.length - 1)) {
              i = 0;
            } else
              i++;
            mapMarkerDatastoreRoutingPath
                ?.createCircleMarkerFromLatLong(latLongList[i]);
          },
          elevation: 2.0,
          fillColor: Colors.white,
          child: const Icon(Icons.add_location_alt),
          padding: const EdgeInsets.all(10.0),
          shape: const CircleBorder(),
          constraints: const BoxConstraints(),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        ),
      ),
    );
  }
}
