import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DirectionOverlay extends StatefulWidget {
  DirectionOverlay({required Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => DirectionOverlayState();
}

/////////////////////////////////////////////////////////////////////////////

class DirectionOverlayState extends State<DirectionOverlay>
    with SingleTickerProviderStateMixin {
  final double toolbarSpacing = 15;

  int i = 0;

  late AnimationController _fadeAnimationController;

  @override
  DirectionOverlay get widget => super.widget;

  bool visibility = false;
  void updateVisibility() {
    setState(() {
      visibility = !visibility;
    });
  }

  @override
  void initState() {
    _fadeAnimationController = AnimationController(
      duration: const Duration(milliseconds: 1000),
      //value: 1,
      vsync: this,
      //lowerBound: 0,
      //upperBound: 1,
    );
    super.initState();
  }

  @override
  void dispose() {
    _fadeAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _fadeAnimationController.forward();
    if (visibility) {
      return Positioned(
        bottom: toolbarSpacing,
        right: toolbarSpacing,
        top: toolbarSpacing,
        left: toolbarSpacing,
        child: FadeTransition(
          opacity: _fadeAnimationController,
          child: new GestureDetector(
            onTap: () {
              setState(() {
                print(directionList.length);
                if (i >= (directionList.length - 1)) {
                  i = 0;
                } else
                  i++;
              });
            },
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  //---------------------------Overlay--------------------------->
                  Center(
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color(0xfffdfdfb),
                        border: Border.all(
                          color: const Color(0xfffdfdfb),
                          width: 8,
                        ),
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.shade700.withOpacity(0.5),
                            spreadRadius: 3,
                            blurRadius: 5,
                            offset: const Offset(0, 3),
                            // changes position of the shadow
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          directionList[i].icon,
                          Expanded(
                            child: Text(
                              directionList[i].description,
                              textAlign: TextAlign.center,
                              textScaleFactor: 1.6,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  //------------------------------------------------------------->
                ]),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  List<DirectionDescription> directionList = [
    new DirectionDescription(
        icon: const Icon(CupertinoIcons.arrow_turn_up_left, size: 100),
        description: "turn left in 100m"),
    new DirectionDescription(
        icon: const Icon(CupertinoIcons.arrow_turn_up_right, size: 100),
        description: "turn right in 200m"),
    new DirectionDescription(
        icon: const Icon(CupertinoIcons.arrow_uturn_down, size: 100),
        description: "please turn around"),
  ];
}

class DirectionDescription {
  String description;
  Icon icon;

  DirectionDescription({required this.description, required this.icon});
}
