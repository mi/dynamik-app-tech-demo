import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:mapsforge_example/map-file-data.dart';
import 'package:mapsforge_example/map-list.dart';

class preStart extends StatefulWidget {
  final List<MapFileData> map_file_data_list;

  preStart(this.map_file_data_list);

  @override
  State<preStart> createState() => _preStartState();
}

class _preStartState extends State<preStart> {
  bool _LiftSwitch = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppbar() as PreferredSizeWidget,
      body: _buildBody(context),
    );
  }

  Widget _buildAppbar() {
    return AppBar(title: const Text("Einstellungen"));
  }

  Widget _buildBody(context) {
    return Column(
      children: [
        Row(
          children: [
            const Text("Bevozuge Aufzüge"),
            Switch(
                value: _LiftSwitch,
                onChanged: (bool value) async {
                  await Hive.openBox("settings");
                  await Hive.box("settings").put("lift", value);
                  setState(() {
                    _LiftSwitch = value;
                  });
                })
          ],
        ),
        // Row(
        //   children: [
        //     BleWidget(),
        //   ],
        // ),
        Row(
          children: [
            Center(
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                MapList(widget.map_file_data_list)));
                  },
                  child: const Text("Weiter")),
            )
          ],
        )
      ],
    );
  }
}
