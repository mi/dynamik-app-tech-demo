import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mapsforge_flutter/core.dart';
import 'package:mapsforge_flutter/maps.dart';

import '../map-file-data.dart';

import 'package:mapsforge_example/filemgr.dart';
import 'package:mapsforge_example/pathhandler.dart';
import 'package:mapsforge_flutter/src/utils/layerutil.dart';
import 'package:mapsforge_flutter/src/model/tile.dart';
import 'package:mapsforge_flutter/src/datastore/datastorereadresult.dart';

///
/// These classes are for debugging purposes only.
///
class ViewPage extends StatelessWidget {
  final MapFileData mapFileData;
  final ViewModel viewModel;

  List<Tile>? tileList;
  MapViewPosition? mapViewPosition;
  BoundingBox? boundingBoxView;

  ViewPage(this.mapFileData, this.viewModel,
      {this.mapViewPosition, this.tileList, this.boundingBoxView}) {
    mapViewPosition = viewModel.mapViewPosition;
    tileList = LayerUtil.getTiles(
        viewModel, mapViewPosition!, DateTime.now().millisecondsSinceEpoch);
    boundingBoxView =
        mapViewPosition!.calculateBoundingBox(viewModel.viewDimension!);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loadMapFile(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.data == null)
          return const Center(
            child: CircularProgressIndicator(),
          );
        MapFile mapFile = snapshot.data;
        return ListView(
          children: <Widget>[
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    "Properties of currently displayed view",
                    style: TextStyle(fontWeight: FontWeight.w900, fontSize: 15),
                  ),
                ],
              ),
            ),
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    "General Properties",
                    style: TextStyle(fontWeight: FontWeight.w900),
                  ),
                  const Text(
                    "\nZoomlevel: ",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  Text("Zoomlevel of mapfile: "
                      "\t${mapFile.zoomLevelMin} - ${mapFile.zoomLevelMax},"),
                  Text("Zoomlevel of MapFileHeader "
                      "${mapFile.getMapFileHeader().zoomLevelMinimum} - "
                      "${mapFile.getMapFileHeader().zoomLevelMaximum}, "),
                  Text("Zoomlevel of current view: "
                      "\t${mapViewPosition!.zoomLevel}, "),
                  const Text(
                    "\nIndoorlevel: ",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  Text("Indoorlevel of current view: "
                      "\t${mapViewPosition!.indoorLevel}, "),
                ],
              ),
            ),
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    "Boundingbox",
                    style: TextStyle(fontWeight: FontWeight.w900),
                  ),
                  const Text(
                    "\nBoundingbox of mapfile: ",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  Text("minLat: "
                      "\t${mapFile.boundingBox.minLatitude}, "),
                  //"\t${mapFile.getMapFileInfo().boundingBox.minLatitude} //this is the same
                  Text("maxLat: "
                      "\t${mapFile.boundingBox.maxLatitude}, "),
                  Text("minLong: "
                      "\t${mapFile.boundingBox.minLongitude}, "),
                  Text("maxLong: "
                      "\t${mapFile.boundingBox.maxLongitude}, "),
                  const Text(
                    "\nBoundingbox of current view: ",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  Text("minLat: \t${boundingBoxView!.minLatitude}, "),
                  Text("maxLat: \t${boundingBoxView!.maxLatitude}, "),
                  Text("minLong: \t${boundingBoxView!.minLongitude}, "),
                  Text("maxLong: \t${boundingBoxView!.maxLongitude}, "),
                ],
              ),
            ),
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    "Tiles ",
                    style: TextStyle(fontWeight: FontWeight.w900),
                  ),
                  const Text(
                    "\nSize of tiles in pixel: ",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  Text("${mapFile.getMapFileInfo().tilePixelSize}"),
                  const Text(
                    "\nList of tiles in view: ",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  Text(_tileListText()),
                ],
              ),
            ),
            new FutureBuilder<DatastoreReadResult>(
              future: _getDatastoreReadResult(mapFile),
              builder: (BuildContext context,
                  AsyncSnapshot<DatastoreReadResult> snapshot) {
                if (snapshot.data == null)
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                else {
                  DatastoreReadResult? datastoreReadResult = snapshot.data;
                  return Wrap(children: <Widget>[
                    Card(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const Text(
                            "Points of interest in current view:\n",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text('${_poiListText(datastoreReadResult)}')
                        ],
                      ),
                    ),
                    Card(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const Text(
                            "Ways in current view:\n",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text('${_wayListText(datastoreReadResult)}')
                        ],
                      ),
                    ),
                  ]);
                }
              },
            )
          ],
        );
      },
    );
  }

  Future<MapFile> _loadMapFile() async {
    PathHandler pathHandler = await FileMgr().getLocalPathHandler("");
    MapFile mapFile = await MapFile.from(
        pathHandler.getPath(mapFileData.fileName), null, null);
    return mapFile;
  }

  Future<DatastoreReadResult> _getDatastoreReadResult(MapFile mapFile) async {
    Tile upperLeft = tileList![0], lowerRight = tileList![0];
    tileList!.forEach((Tile tile) {
      if (upperLeft.tileX >= tile.tileX && upperLeft.tileY >= tile.tileY)
        upperLeft = tile;
      if (lowerRight.tileX <= tile.tileX && lowerRight.tileY <= tile.tileY)
        lowerRight = tile;
    });
    return await mapFile.readLabels(upperLeft, lowerRight);
  }

  String _tileListText() {
    String tileListText = ('');
    tileList!.forEach((Tile tile) {
      tileListText = ('$tileListText' + '\n${tile.toString()}');
    });
    return tileListText;
  }

  String _poiListText(DatastoreReadResult? datastoreReadResult) {
    String poiListText = ('Number of PointsOfInterest:  '
        '${datastoreReadResult!.pointOfInterests.length}\n\n');

    int i = 1;
    for (final poi in datastoreReadResult.pointOfInterests) {
      if (i > 90) {
        poiListText =
            ('$poiListText' + '. . . (there are more than 90 poi)\n\n');
        break;
      }
      //if (POI.tags.contains(Tag('door', 'yes'))) {}    //filter Tags
      poiListText = ('$poiListText' + 'Poi $i: ${poi.toString()}\n\n');
      i++;
    }

    return poiListText;
  }

  String _wayListText(DatastoreReadResult? datastoreReadResult) {
    String wayListText = ('Number of ways:  '
        '${datastoreReadResult!.ways.length}\n\n');
    int i = 1;

    for (final way in datastoreReadResult.ways) {
      if (i > 90) {
        wayListText =
            ('$wayListText' + '. . . (there are more than 90 ways)\n\n');
        break;
      }
      //if (way.tags.contains(Tag('indoor', 'room'))) {} //filter Tags
      wayListText = ('$wayListText' + 'way $i: ${way.tags.toString()}\n\n');

      i++;
    }

    return wayListText;
  }

  String formatMsToDatetimeMs(int? ms) {
    if (ms == null) return "";
    if (ms == 0) return "";
    DateTime date = new DateTime.fromMillisecondsSinceEpoch(ms, isUtc: true);
    var format = DateFormat("yyyy-MM-dd HH:mm:ss-SSS");
    return format.format(date);
  }

  String formatLatLong(ILatLong? latLong) {
    if (latLong == null) return "Unknown";
    return "${latLong.latitude.toStringAsPrecision(6)} "
        "/ ${latLong.longitude.toStringAsPrecision(6)}";
  }

  String formatBoundingbox(BoundingBox boundingBox) {
    return "${boundingBox.maxLatitude.toStringAsPrecision(6)} "
        "/ ${boundingBox.minLongitude.toStringAsPrecision(6)} "
        "- ${boundingBox.minLatitude.toStringAsPrecision(6)} "
        "/ ${boundingBox.maxLongitude.toStringAsPrecision(6)}";
  }
}
