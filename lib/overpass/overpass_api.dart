import 'dart:convert';
import 'package:xml/xml.dart';
import 'package:http/http.dart';
import 'overpass_query.dart';
import '../mapElements/map_elements.dart';

class OverpassApi {
  static String _apiUrl = 'overpass-api.de';
  static String _path = '/api/interpreter';

  Future<MapElements?> sendRequest(
      {required QueryArea? queryArea,
      required QueryBoundingBox? queryBoundingBox,
      required Map<String, dynamic>? filter}) async {
    Request request = Request('GET', Uri.https(_apiUrl, _path));

    if (queryArea != null)
      request.bodyFields = new OverpassQuery(
              outputFileFormat: 'json',
              outputResponseData: 'out',
              timeout: 20,
              filterTags: filter,
              queryArea: queryArea)
          .generateQuery();
    else if (queryBoundingBox != null)
      request.bodyFields = new OverpassQuery(
              outputFileFormat: 'json',
              outputResponseData: 'out',
              timeout: 20,
              filterTags: filter,
              queryBoundingBox: queryBoundingBox)
          .generateQuery();
    else
      print(
          'ERROR overpass_api.dart: queryLocation and queryBoundingBox are null');

    String responseText = await requestToResponse(request);

    var responseJson = await responseDecode(responseText);

    if (responseJson['elements'] == null) return null;

    return MapElements.fromJson(responseJson['elements'], filter);
  }

  Future<String> requestToResponse(Request request) async {
    String responseText;

    try {
      //first try
      StreamedResponse response =
          await Client().send(request).timeout(const Duration(seconds: 5));
      responseText = await response.stream.bytesToString();
    } catch (exception) {
      print(exception);
      return Future.error(exception);
    }
    return responseText;
  }

  dynamic responseDecode(String responseText) {
    var responseJson;

    try {
      responseJson = jsonDecode(responseText);
    } catch (exception) {
      String error = '';
      final document = XmlDocument.parse(responseText);
      final paragraphs = document.findAllElements("p");

      paragraphs.forEach((element) {
        if (element.text.trim() == '') {
          return;
        }

        error += '${element.text.trim()}';
      });

      return Future.error(error);
    }
    return responseJson;
  }
}
