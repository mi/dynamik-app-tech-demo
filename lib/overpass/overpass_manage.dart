import 'overpass_api.dart';
import 'overpass_query.dart';
import '../mapElements/map_elements.dart';

class OverpassManage {
  OverpassManage({
    required this.overpassApi,
  });

  final OverpassApi overpassApi;

  Future<MapElements?> getElementsInArea(
      {required double longitude,
      required double latitude,
      required double radius,
      Map<String, dynamic>? filter}) async {
    filter ??= {};

    MapElements? fetchResult = await this.overpassApi.sendRequest(
        queryBoundingBox: null,
        queryArea: new QueryArea(
            longitude: longitude, latitude: latitude, radius: radius),
        filter: filter);

    return fetchResult;
  }

  Future<MapElements?> getElementsInBoundingBox(
      {required double minLatitude,
      required double minLongitude,
      required double maxLatitude,
      required double maxLongitude,
      Map<String, dynamic>? filter}) async {
    filter ??= {};

    MapElements? fetchResult = await this.overpassApi.sendRequest(
        queryBoundingBox: new QueryBoundingBox(
            south: minLatitude,
            west: minLongitude,
            north: maxLatitude,
            east: maxLongitude),
        queryArea: null,
        filter: filter);

    return fetchResult;
  }
}
