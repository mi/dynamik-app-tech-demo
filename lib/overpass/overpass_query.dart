class OverpassQuery {
  String outputFileFormat;
  String outputResponseData;
  int timeout;
  Map<String, dynamic>? filterTags;
  QueryArea? queryArea;
  QueryBoundingBox? queryBoundingBox;

  OverpassQuery(
      {required this.outputFileFormat,
      required this.outputResponseData,
      required this.timeout,
      required this.filterTags,
      this.queryArea,
      this.queryBoundingBox})
      : assert((queryArea != null) ^ (queryBoundingBox != null));
  //needs either Area (LatLon with Radius)
  //or BBox (minLat, minLon, maxLat, maxLon) for query

  Map<String, String> generateQuery() {
    String areaString;

    if (queryArea != null)
      areaString = '$queryArea';
    else
      areaString = '$queryBoundingBox';

    String tagString = '';

    print("$filterTags"); //for debugging
    filterTags!.forEach((key, value) {
      if (key != "null" && value != "null")
        tagString += "[\"$key\"=\"$value\"]";
      else if (value == "null" && key != "null")
        tagString += "[\"$key\"]";
      else
        tagString += "[]";
    });

    print("$tagString"); //for debugging

    String query = '[out:$outputFileFormat][timeout:$timeout];'
        '(node$tagString$areaString;way$tagString$areaString;);out;>;out;';

    return <String, String>{'data': query};
  }
}

class QueryArea {
  final double longitude;
  final double latitude;
  final double radius;

  QueryArea(
      {required this.longitude, required this.latitude, required this.radius});

  @override
  String toString() {
    return '(around:${radius},${latitude},${longitude})';
  }
}

class QueryBoundingBox {
  final double south; //minLat
  final double west; //minLon
  final double north; //maxLat
  final double east; //maxLon

  QueryBoundingBox({
    required this.south,
    required this.west,
    required this.north,
    required this.east,
  });

  @override
  String toString() {
    return '(bbox:${south},${west},${north},${east})';
  }
}
