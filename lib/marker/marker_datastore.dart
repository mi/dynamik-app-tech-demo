import 'dart:async';

import 'package:mapsforge_flutter/core.dart';
import 'package:mapsforge_flutter/marker.dart';

import '../mapElements/map_elements.dart';
import 'marker_tags.dart';

class MapMarkerDatastore extends MarkerDataStore {
  final SymbolCache symbolCache;

  MapMarkerDatastore({required this.symbolCache}) {}

  Future<void> createMarkerFromMapElements(MapElements? mapElements) async {
    PoiMarker poiMarker;
    PathMarker pathMarker;
    PolygonMarker polygonMarker;

    if (mapElements != null) {
      mapElements.pois.forEach((element) async {
        poiMarker = _createPoiMarker(
            element.latitude,
            element.longitude,
            MarkerTags().getIconSrcFromMarkerTagList(element.tags),
            MarkerTags().getCaptionFromTags(element.tags));
        addMarker(poiMarker);
        await poiMarker.initResources(symbolCache);

        print('Element poi: ${element.longitude} ${element.latitude} '
            '${element.tags}'); //for debugging
      });

      mapElements.ways.forEach((element) async {
        print('Element way: ${element.tags}'); //for debugging

        if (element.path[0] == element.path[element.path.length - 1]) {
          //TODO: use the bool polygon from the class Way

          polygonMarker = _createPolyMarker(
              element.path,
              MarkerTags().getIconSrcFromMarkerTagList(element.tags),
              MarkerTags().getCaptionFromTags(element.tags));
          addMarker(polygonMarker);
          await polygonMarker.initResources(symbolCache);
        } else {
          pathMarker = _createPathMarker(element.path);
          addMarker(pathMarker);
          //await pathMarker.initResources();
        }
      });
      setRepaint();
    } else
      print("no data found");
  }

  PoiMarker _createPoiMarker(
      double latitude, double longitude, String iconSrc, String caption) {
    return new PoiMarker(
      src: iconSrc,
      latLong: new LatLong(latitude, longitude),
      markerCaption: new MarkerCaption(text: caption, fontSize: 6.0),
    );
    //setRepaint();
  }

  PolygonMarker _createPolyMarker(
      List<ILatLong> path, String iconSrc, String caption) {
    PolygonMarker marker = new PolygonMarker(
      markerCaption: new MarkerCaption(text: caption),
    );
    marker.path = path;
    return marker;
  }

  PathMarker _createPathMarker(List<ILatLong> path) {
    PathMarker marker = new PathMarker();
    marker.path = path;
    return marker;
  }

  static void clearMarkerDataStore(MarkerDataStore MarkerDataStore) {
    MarkerDataStore.clearMarkers();
    MarkerDataStore.setRepaint();
  }

  /*
  static void deleteAllMarker(List<IMarkerDataStore> listMarkerDataStore) {
    listMarkerDataStore.clear();
  }
  */
}
