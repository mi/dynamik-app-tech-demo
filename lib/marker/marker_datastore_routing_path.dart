import 'dart:async';

import 'package:mapsforge_flutter/core.dart';
import 'package:mapsforge_flutter/marker.dart';

import '../mapElements/map_elements.dart';
import 'marker_tags.dart';

class MapMarkerDatastoreRoutingPath extends MarkerDataStore {
  final SymbolCache symbolCache;

  MapMarkerDatastoreRoutingPath({required this.symbolCache}) {}

  Future<void> createRouteMarkerFromLatLongs(List<ILatLong> latLongs) async {
    PathMarker pathMarker;

    pathMarker = _createPathMarker(latLongs);
    addMarker(pathMarker);
    //await pathMarker.initResources();

    setRepaint();
  }

  Future<void> createCircleMarkerFromLatLong(ILatLong latLong) async {
    CircleMarker circleMarker;

    circleMarker = _createCircleMarker(latLong);
    addMarker(circleMarker);
    //await circleMarker.initResources(symbolCache);

    setRepaint();
  }

  PathMarker _createPathMarker(List<ILatLong> path) {
    PathMarker marker =
        new PathMarker(strokeWidth: 4.0, strokeColor: 0xffbb4433);
    marker.path = path;
    setRepaint();
    return marker;
  }

  CircleMarker _createCircleMarker(ILatLong latLong) {
    CircleMarker marker =
        new CircleMarker(center: latLong, radius: 5, fillColor: 0xffee5522);
    setRepaint();
    return marker;
  }

  static void clearMarkerDataStore(MarkerDataStore MarkerDataStore) {
    MarkerDataStore.clearMarkers();
    MarkerDataStore.setRepaint();
  }

  /*
  static void deleteAllMarker(List<IMarkerDataStore> listMarkerDataStore) {
    listMarkerDataStore.clear();
  }
  */
}
