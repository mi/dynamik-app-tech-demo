class MarkerTags {
  //this is not final, but mostly for testing
  //TODO: get suitable icons for ImageSource
  //TODO: add the information if a tag can be used for poi, way, polygon (area), and/or polygon (line)
  Map<String, TagStructure> MarkerTagMap = {
    "null": TagStructure(
        caption: "EVERYTHING",
        tags: {"null": "null"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
    "door": TagStructure(
        caption: "Door",
        tags: {"door": "yes"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/bench.svg"),
    "tree": TagStructure(
        caption: "Tree",
        tags: {"natural": "tree"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
    "bench": TagStructure(
        caption: "Bench",
        tags: {"amenity": "bench"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
    "toilet": TagStructure(
        caption: "Toilet",
        tags: {"amenity": "toilets"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
    "highway_all": TagStructure(
        caption: "All Ways",
        tags: {"highway": "null"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
    "footway": TagStructure(
        caption: "Footway",
        tags: {"highway": "footway"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
    "tertiary": TagStructure(
        caption: "Tertiary",
        tags: {"highway": "tertiary"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
    "steps": TagStructure(
        caption: "Steps",
        tags: {"highway": "steps"},
        ImageSource: "packages/mapsforge_flutter/assets/symbols/volcano.svg"),
  };

  //additional Tags are tags that are descriptive
  //and should mostly be used in combination with the Tags above
  Map<String, additionalTags> additionalTagList = {
    "wheelchair_yes": additionalTags(
        caption: "Wheelchair Accessibility", tags: {"wheelchair": "yes"}),
    "wheelchair_limited": additionalTags(
        caption: "Limited Wheelchair Accessibility",
        tags: {"wheelchair": "limited"}),
    //values of the key "wheelchair" can be: yes, no, limited, only
    "level_0": additionalTags(caption: "Level 0", tags: {"level": "0"}),
    "level_1": additionalTags(caption: "Level 1", tags: {"level": "1"}),
    "level_2": additionalTags(caption: "Level 2", tags: {"level": "2"}),
    "level_3": additionalTags(caption: "Level 3", tags: {"level": "3"}),
    "floor_asphalt": additionalTags(
        caption: "Floor Material: Asphalt",
        tags: {"surface": "asphalt"}), //floor:material
    "floor_plaster": additionalTags(
        caption: "Floor Material: Plaster", tags: {"surface": "plaster"}),
    "floor_paving_stones": additionalTags(
        caption: "Floor Material: Paving Stones",
        tags: {"surface": "paving_stones"}),
  };

  //searches the captions of the TagLists
  Map<String, dynamic>? getTagsFromTagList(List<String> identifier) {
    Map<String, dynamic>? returnMap = {};
    identifier.forEach((element) {
      MarkerTagMap.forEach((key, value) {
        if (key == element) returnMap.addAll(value.tags);
      });
      additionalTagList.forEach((key, value) {
        if (key == element) returnMap.addAll(value.tags);
      });
    });

    return returnMap;
  }

  String getIconSrcFromMarkerTagList(Map<String, dynamic>? tags) {
    //not made to match multiple tags
    String? iconSrc;
    tags?.forEach((keyTags, valueTags) {
      MarkerTags().MarkerTagMap.forEach((keyID, valueTagStructureList) {
        if (valueTagStructureList.tags.containsKey(keyTags) &&
            valueTagStructureList.tags[keyTags] == valueTags) {
          iconSrc = valueTagStructureList.ImageSource;
        }
      });
    });
    return iconSrc ??
        "packages/mapsforge_flutter/assets/symbols/custom/tourist/view_point.svg";
  }

  String getCaptionFromId(String identifier) {
    String caption = "none";
    MarkerTagMap.forEach((key, value) {
      if (key == identifier) caption = value.caption;
    });
    if (caption == "none") {
      additionalTagList.forEach((key, value) {
        if (key == identifier) caption = value.caption;
      });
    }
    return caption;
  }

  String getCaptionFromTags(Map<String, dynamic>? tags) {
    String caption = "none";
    tags?.forEach((keyTags, valueTags) {
      MarkerTags().MarkerTagMap.forEach((keyID, valueTagAndSrcList) {
        if (valueTagAndSrcList.tags.containsKey(keyTags) &&
            valueTagAndSrcList.tags[keyTags] == valueTags) {
          caption = valueTagAndSrcList.caption;
        }
      });
      if (caption == "none") {
        MarkerTags().additionalTagList.forEach((keyID, valueTagAndSrcList) {
          if (valueTagAndSrcList.tags.containsKey(keyTags) &&
              valueTagAndSrcList.tags[keyTags] == valueTags) {
            caption = valueTagAndSrcList.caption;
          }
        });
      }
    });
    return caption;
  }
}

class TagStructure {
  String caption;
  Map<String, dynamic> tags;
  String? ImageSource;

  TagStructure(
      {required this.caption, required this.tags, required this.ImageSource});
}

class additionalTags {
  String caption;
  Map<String, dynamic> tags;

  additionalTags({required this.caption, required this.tags});
}

//This is a list of Tags that maybe interesting for indoor mapping and accessibility
/*Map<String, dynamic>? tags = {
    "door": "yes",
    "level": "1",
    "level:ref": "EG",

    "amenity": "toilets",
    "amenity": "bench",
    "amenity": "table",
    "amenity": "chair",             //static, immovable
    "amenity": "vending_machine",
    "amenity": "food_court",
    "amenity": "parking",
    "amenity": "shelter",
    "amenity": "waste_basket",
    "amenity": "device_charging_station",
    "amenity": "",

    "wheelchair": "yes",    //no,limited,only
    "wheelchair:description": "",
    "wheelchair:entrance_width": "",
    "wheelchair:step_height": "",
    "toilets:wheelchair": "",
    "ramp:wheelchair": "yes",

    "highway": "steps",
    "highway": "elevator",
    "highway": "pedestrian",
    "highway": "footway",
    "highway": "corridor",
    "step_count": "",
    "barrier": "step",
    "tactile_paving": "yes",

    "barrier": "handrail",
    "tactile_writing:braille:lg": "yes",

    "addr:housenumber": "",
    "addr:street": "",

    "floor:material": "",
    "surface": "",
  };*/
